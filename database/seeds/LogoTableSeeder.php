<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LogoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('logos')->insert([
            'id' => '1',
            'nama' => 'Nama Intansi Xyz',
            'about' => 'Jujur dan Adil',
            'photo' => 'cook.jpg'
        ]);
    }
}
